## UI setup
1. In a terminal session, navigate to the root directory of the repository.
1. Install the UI dependencies:  
   `npm install`
1. Run the UI in development mode:  
   `npm run dev`

Access the UI using the URL output to the console. E.g.
> Listening on: http://localhost:8080/



### UI tests
Cypress is used to run end-to-end browser automation tests on the UI.

Tests are created as `*.spec.js` files in the `/cypress/integration` folder.

**N.B.** The UI tests rely on the UI dev server will fail if it's not available at http://localhost:8080/.

From a new terminal and in the repo root folder, start the Cypress test runner with:  
`npm run test`
