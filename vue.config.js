const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')
const webpack = require('webpack')
const version = JSON.parse(packageJson).version || 0

module.exports = {
  configureWebpack: {
    devtool: 'source-map',
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          PACKAGE_VERSION: '"' + version + '"'
        }
      })
    ]
  },
  filenameHashing: false,
  publicPath: process.env.VUE_APP_PUBLIC_PATH || '/',
  css: {
    extract: true
  }
}
