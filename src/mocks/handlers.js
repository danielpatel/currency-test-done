import currency from '../../currencies.json'
import { rest } from 'msw'

const {
  VUE_APP_API
} = process.env

export default [

  rest.get(
    VUE_APP_API,
    (req, res, ctx) => {
      return res(ctx.json({
        success: true,
        mocked: true
      }))
    }
  ),

  rest.get(
        `${VUE_APP_API}/currency-data`,
        (req, res, ctx) => {
          const data = currency
          return res(ctx.json({ data }))
        }
  )
]
