import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import store from './store'

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

Vue.config.productionTip = false

if (process.env.NODE_ENV === 'development') {
  const { worker } = require('./mocks/browser')
  console.debug('Plugin: api-mocking initialisation...')
  worker.start({
    onUnhandledRequest: 'bypass'
  }).then(() => {
    console.debug('MSR - worker started')
  })
}

const app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

if (window.Cypress) {
  window.app = app
}
