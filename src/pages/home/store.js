import axios from 'axios'
import { DateTime } from 'luxon'

const {
  VUE_APP_API
} = process.env

const state = () => ({
  currencyData: null,
  dateData: null,
  dates: null
})

const actions = {
  async INITIAL ({ commit }) {
    const initial = () => axios.get(`${VUE_APP_API}/currency-data`)
      .then(response => {
        if (response.status === 200 && response.data) {
          const tmpArray = Object.keys(response.data.data).map(key => response.data.data[key])
          const dateArray = [
            {
              value: null,
              text: 'Please select a date'
            }
          ]
          tmpArray.forEach((obj) => {
            const tmpData = DateTime.fromSeconds(obj.timestamp)
            obj.date = tmpData.toLocaleString(DateTime.DATE_MED)
            dateArray.push({
              value: obj.timestamp,
              text: obj.date
            })
          })
          commit('CREATE_DATES', dateArray)
          commit('CHANGE_DATA', tmpArray)
        } else commit('CHANGE_DATA', [])
      })
      .catch(() => commit('CHANGE_DATA', []))

    initial().then(res => res)
  },
  async GET_DATE_DATA ({ commit, state }, newDate) {
    const getPercentageChange = (current, previous) => {
      const value = current - previous
      const percentage = (value / current) * 100
      return parseFloat(percentage.toFixed(2))
    }

    const dateData = () => {
      if (!newDate) return commit('CHANGE_DATE', null)
      const currentDate = state.currencyData.find(({ timestamp }) => timestamp === newDate)
      const previousDate = state.currencyData.find(({ date }) => date === DateTime.fromSeconds(newDate).minus({ days: 1 }).toLocaleString(DateTime.DATE_MED))
      const result = {}
      result.base = currentDate.base
      result.rates = []
      for (const [key, value] of Object.entries(currentDate.rates)) {
        const tmpObj = {}
        tmpObj.code = key || 'N/A'
        tmpObj.currentRate = value || 'N/A'
        tmpObj.previousRate = previousDate ? previousDate.rates[`${key}`] : 'N/A'
        tmpObj.percentageChange = previousDate ? getPercentageChange(tmpObj.currentRate, tmpObj.previousRate) : 'N/A'
        result.rates.push(tmpObj)
      }
      commit('CHANGE_DATE', result)
    }

    dateData()
  }
}

const mutations = {
  CHANGE_DATA (state, payload) {
    state.currencyData = payload
  },
  CREATE_DATES (state, payload) {
    state.dates = payload
  },
  CHANGE_DATE (state, payload) {
    state.dateData = payload
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
