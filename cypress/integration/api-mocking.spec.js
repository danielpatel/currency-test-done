describe('API Mocking', () => {
  before(() => cy.visit('http://localhost:8080/'))

  it('API should return status', () => {
    cy.window().then(win => {
      cy.wrap({ func: () => win.fetch('/api').then(response => response.json()) })
        .invoke('func')
        .then(json => {
          expect(json.success).to.equal(true)
          expect(json.mocked).to.equal(true)
        })
    })
  })

  it('API /currency-data should return some data', () => {
    cy.window().then(win => {
      cy.wrap({ func: () => win.fetch('/api/currency-data').then(response => response.json()) })
        .invoke('func')
        .then(json => {
          expect(json.data).to.be.an('object')
        })
    })
  })
})
