describe('Section: Currency', () => {
  before(() => cy.visit('http://localhost:8080/'))

  it('should load the correct page', () => {
    cy.get('.page.home')
  })

  it('should include date selection', () => {
    cy.get('.date-selection')
  })

  it('should include typehead filter', () => {
    cy.get('.date-selection').select('2 Jan 2017')
    cy.get('.typeahead-selection')
  })

  it('should include currency table', () => {
    cy.get('.date-selection')
  })
})
